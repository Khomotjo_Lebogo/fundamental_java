/**
 * Created by intern on 9/18/15.
 */

import java.util.Scanner;

public class BubbleSort {
    public static void main(String args[]) {

        int number, arrayNum, data, swap;
        Scanner in = new Scanner(System.in);

        System.out.println("Input number of integers to sort");
        number = in.nextInt();

        int array[] = new int[number];

        System.out.println("Enter " + number + " integers");

        for (arrayNum = 0; arrayNum < number; arrayNum++)
            array[arrayNum] = in.nextInt();

        for (arrayNum = 0; arrayNum < (number - 1); arrayNum++) {
            for (data = 0; data < number - arrayNum - 1; data++) {
                if (array[data] > array[data + 1]) /* For descending order use < */ {
                    swap = array[data];
                    array[data] = array[data + 1];
                    array[data + 1] = swap;
                }
            }
        }

        System.out.println("Sorted list of numbers");

        for (arrayNum = 0; arrayNum < number; arrayNum++)
            System.out.println(array[arrayNum]);
    }
}