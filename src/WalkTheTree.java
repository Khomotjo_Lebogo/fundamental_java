/**
 * Created by intern on 9/22/15.
 */

import java.io.File;

public class WalkTheTree {
    public static void main(String[] args) {
        walkin(new File("/Users/intern/Desktop/fundamental/fundamental_java/src"));
    }

    public static void walkin (File dir) {
        String pattern = ".txt";

        File listFile[] = dir.listFiles();
        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    walkin(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(pattern)) {
                        System.out.println(listFile[i].getPath( ));
                    }
                }
            }
        }
    }
}