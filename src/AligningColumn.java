/**
 * Created by intern on 9/30/15.
 */
public class AligningColumn {
    public static void main(String[] args) {
        String[] information = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line",
                "are$delineated$by$a$single$'dollar'$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                "column$are$separated$by$at$least$one$space.",
                "Further,$allow$for$each$word$in$a$column$to$be$either$left",
                "justified,$right$justified,$or$center$justified$within$its$column."};

        String[] splitedInfo;
        System.out.println("left justified column\n");
        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String i : splitedInfo) {
                System.out.printf("%-15s", i);

            }
            System.out.println();

        }

        System.out.println("\nRight justified column \n");
        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String j : splitedInfo) {
                System.out.printf("%15s", j);

            }
            System.out.println();

        }


       System.out.println("\nCenter justified column\n");

        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String k : splitedInfo ) {
                System.out.printf("%s",k);


            }

            System.out.println();
        }


    }

}

