/**
 * Created by intern on 9/22/15.
 */
import java.util.Scanner;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;

public class ReverseIt {


    public static void main(String[] args) {



        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter a string you want to reverse : ");
        String name = input.nextLine();
        String reversedName = "";


        for (int j = name.length() - 1; j >= 0; j--) {
            reversedName = reversedName + name.charAt(j);

        }

        System.out.println("\nNormal word is : " + name + " \nReverse word is : " + reversedName);

        String[] strDays = new String[]{"Sunday", "Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday"};

        List<String> list = Arrays.asList(strDays);
        Collections.reverse(list);
        strDays = (String[]) list.toArray();

        System.out.println("String array reversed");

        for (int i = 0; i < strDays.length; i++) {
            System.out.println(strDays[i]);
        }

    }
}



