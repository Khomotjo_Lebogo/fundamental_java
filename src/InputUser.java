/**
 * Created by intern on 9/17/15.
 */

import java.util.Scanner;

public class InputUser {
    public static void main(String args[]) {
        int age;
        String name;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a your name : ");
        name = in.nextLine();
        System.out.println("Your are " + name);
        System.out.println("Enter an your age : ");
        age = in.nextInt();
        System.out.println("you are " + age + " years old ");
    }
}

