/**
 * Created by intern on 10/2/15.
 */

import java.io.*;

import java.util.Scanner;

public class AddFile {

    static public void main(String args[]) throws FileNotFoundException {
        String FILE_PATH = "/Users/intern/Desktop/fundamental/fundamental_java/src/data.txt";
        int total = 0;

        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(FILE_PATH)));
            String line;
            System.out.println("Numbers in file are:");
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                total += Integer.parseInt(line);
            }
            in.close();
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage() + " is not numeric");
            System.exit(1);
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't open " + e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.out.println("IO error");
            System.exit(1);
        }

        System.out.println("\nsum = " + total);

    }
}