/**
 * Created by intern on 9/22/15.
 */import java.util.Scanner;
public class CommasAnd {

    public static String quibble(String[] words) {
        String qText = "{";
        for(int wIndex = 0; wIndex < words.length; wIndex++) {
            qText += words[wIndex] + (wIndex == words.length-1 ? "" :
                    wIndex == words.length-2 ? " and " :
                            ", ");

        }
        qText += "}";
        return qText;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println(" USE UPPERCASE \n");

        System.out.println("Enter your name ");
        String name = input.nextLine();

        System.out.println("Enter your surname");
        String surname = input.nextLine();

        System.out.println("Enter your othername");
        String otherName  = input.nextLine();

        System.out.println(quibble(new String[]{}));
        System.out.println(quibble(new String[]{name}));
        System.out.println(quibble(new String[]{name, surname}));
        System.out.println(quibble(new String[]{name, otherName, surname}));

    }
}