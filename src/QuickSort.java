/**
 * Created by intern on 10/1/15.
 */
public class QuickSort {

        private static void quicksort(int[] a, int lo, int hi) {

            int lowerIndex = lo, upperIndex = hi;

            // comparison element x
            int x = a[(lo + hi) / 2];

            // partition
            do {
                while (a[lowerIndex] < x)
                    lowerIndex++;
                while (a[upperIndex] > x)
                    upperIndex--;
                if (lowerIndex <= upperIndex) {
                    // XOR swap does not work when a[i] == a[j]
                    if (a[lowerIndex] != a[upperIndex]) {
                        a[lowerIndex] ^= a[upperIndex];
                        a[upperIndex] ^= a[lowerIndex];
                        a[lowerIndex] ^= a[upperIndex];
                    }
                    lowerIndex++;
                    upperIndex--;
                }
            } while (lowerIndex <= upperIndex);

            // recursion
            if (lo < upperIndex)
                quicksort(a, lo, upperIndex);
            if (lowerIndex < hi)
                quicksort(a, lowerIndex, hi);
        }

        public static void main(String[] args) {
            int[] array = { 3, 5, -2, 4, 0, -1, -7, 6 };
            quicksort(array, 0, array.length - 1);

            for (int i = 0; i < array.length; ++i) {
                System.out.print(array[i]);
                if (i + 1 < array.length)
                    System.out.print(", ");
            }
            System.out.println();
        }
    }

