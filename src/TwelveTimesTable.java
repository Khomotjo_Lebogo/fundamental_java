import com.sun.javafx.runtime.SystemProperties;

/**
 * Created by intern on 9/23/15.
 */
public class TwelveTimesTable {

    public static void main(String[] args) {

        for (int number = 1; number <= 12; number++) {

            for (int number1 = 1; number1 <= 12; number1++) {
                int result;
                result = number * number1;

                System.out.println(number + " x " + number1 + " = " + result);
            }
            System.out.println("\n");
        }
    }
}





