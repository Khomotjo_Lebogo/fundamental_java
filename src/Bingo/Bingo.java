package bingo;

/**
 * Created by intern on 10/6/15.
 */

import java.util.ArrayList;
import java.util.Collections;

public class Bingo {


    /* 0-75, where 0 = free space and 1-75 are actual numbers */
    static final int numberSetSize = 76;

    static final int boardSize = 25;

    static final int columnRange = 15;

    static final int colSize = 5;

    static final int cardsToPlay = 500;
    static final int gamesToPlay = 100;

    public static void main(String[] args) {
        int gamesPlayed = 0;
        int totalNumbersCalled = 0;


        ArrayList<BingoCard> cards = new ArrayList<BingoCard>(cardsToPlay);
        for (int i = 0; i < cardsToPlay; ++i) {
            cards.add(new BingoCard());
        }



        ArrayList<Integer> numbersCalled = new ArrayList<Integer>(numberSetSize - 1);
        for (int i = 1; i < numberSetSize; ++i) {
            numbersCalled.add(i);
        }


        while (gamesPlayed < gamesToPlay) {

            for (BingoCard card : cards) {
                card.resetCard();
            }
            Collections.shuffle(numbersCalled);

            int i ;
            gameLoop:
            for (i = 0; i < numbersCalled.size(); ++i) {

                for (BingoCard card : cards) {

                    card.mark(numbersCalled.get(i));
                    if (card.checkForBingo()) {

                        break gameLoop;
                    }
                }
            }

            totalNumbersCalled += i;
            ++gamesPlayed;
        }

        System.out.println("Total games played: " + gamesToPlay);
        System.out.println("Total numbers called: " + totalNumbersCalled);
        System.out.println("Average numbers called before a BINGO: "
                + (double) totalNumbersCalled / gamesToPlay);

    }


}