/**
 * Created by intern on 10/9/15.
 */

import java.util.Random;
import java.util.Scanner;


public class Exercise {
    public static void main(String[] args) {
        Exercise game = new Exercise();
        game.startGame();

    }
    private User user;
    private Computer computer;
    private int userScore;
    private int computerScore;
    private int numberOfGames;


    private enum Move {
        ROCK, PAPER, SCISSORS;

        public int compareMoves(Move otherMove) {
            if (this == otherMove)
                return 0;
            switch (this) {
                case ROCK:
                    return (otherMove == SCISSORS ? 1 : -1);
                case PAPER:
                    return (otherMove == ROCK ? 1 : -1);
                case SCISSORS:
                    return (otherMove == PAPER ? 1 : -1);
            }

            return 0;
        }
    }

    private class User {
        private Scanner inputScanner;

        public User() {
            inputScanner = new Scanner(System.in);
        }

        public Move getMove() {
            System.out.print("Rock, paper, or scissors? ");


            String userInput = inputScanner.nextLine();
            userInput = userInput.toUpperCase();
            char firstLetter = userInput.charAt(0);

            if (firstLetter == 'R' || firstLetter == 'P' || firstLetter == 'S') {

                switch (firstLetter) {
                    case 'R':
                        return Move.ROCK;
                    case 'P':
                        return Move.PAPER;
                    case 'S':
                        return Move.SCISSORS;
                }
            }

            return getMove();
        }

        public boolean playAgain() {
            System.out.print("Do you want to play again?(yes/no) ");
            String userInput = inputScanner.nextLine();
            userInput = userInput.toUpperCase();
            return userInput.charAt(0) == 'Y';
        }
    }

    private class Computer {
        public Move getMove() {
            Move[] moves = Move.values();
            Random random = new Random();
            int index = random.nextInt(moves.length);
            return moves[index];
        }
    }

    public Exercise() {
        user = new User();
        computer = new Computer();


    }

    public void startGame() {

        Move userMove = user.getMove();
        Move computerMove = computer.getMove();
        System.out.println("\nYou played " + userMove + ".");
        System.out.println("Computer played " + computerMove + ".\n");

        int compareMoves = userMove.compareMoves(computerMove);
        switch (compareMoves) {
            case 0:
                System.out.println("you draw!");
                break;
            case 1:
                System.out.println(userMove + " beats " + computerMove + ". You won!");
userScore++;
                break;
            case -1:
                System.out.println(computerMove + " beats " + userMove + ". You lost.");
computerScore++;
                break;
        }
        numberOfGames++;
        //System.out.print("\nGame Over!!!");
        if (user.playAgain()) {
            System.out.println();
            startGame();
        } else {
            printGameStats();
        }
    }
    private void printGameStats() {
        int wins = userScore;
        int losses = computerScore;
        int ties = numberOfGames - userScore - computerScore;
        double percentageWon = (wins + ((double) ties) / 2) / numberOfGames;
        System.out.print("\nstats\n");
        System.out.printf("\n|  %3d  |  %3d  |  %3d  |  %6d  |  %7.2f%%  |\n",
                wins, losses, ties, numberOfGames, percentageWon * 100);

    }

}
