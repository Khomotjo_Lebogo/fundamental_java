/**
 * Created by intern on 10/9/15.
 */

import java.util.Random;

public class WarmUpExercise {

    public static void main(String[] args) {

        String[] fruit = {"Apples", "Pears", "Grapes", "Oranges"};
        double[] weight = {0.25, 0.5, 0.2, 0.05};
        double accumulative_sum = 0;
        Random rand = new Random();
        double sum = 0;

        for (int elements = 0; elements < weight.length; elements++) {
            sum = accumulative_sum += weight[elements];
            System.out.println(fruit[elements] + " => " + sum);
        }
        System.out.println("\n");

        double random = rand.nextInt(100)/100d;
        System.out.print("random:" + random);

        for (int element = 0; element < weight.length; element++) {

            if (sum >= weight[element]) {
                System.out.print("\n" + "cumulative probability that's greater than the random number\n");
                System.out.print(fruit[element] +"\t"+ weight[element]);
                break;
            }
        }


    }
}
