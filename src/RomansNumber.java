/**
 * Created by intern on 10/8/15.
 */

import java.util.Scanner;

public class RomansNumber {
    private static int input;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter first roman number: ");
        String firstRom = scan.next();

        System.out.print("Please enter second roman number: ");
        String secondRom = scan.next();

        int sum = RomanToNumber(firstRom) + RomanToNumber(secondRom);
        System.out.println(sum);
        System.out.print(integerToRomanNumeral(sum));

    }

    public static String integerToRomanNumeral(int input) {
        RomansNumber.input = input;
        if (input < 1 || input > 3999)
            return "Invalid Roman Number Value";
        String s = "";
        while (input >= 1000) {
            s += "M";
            input -= 1000;
        }
        while (input >= 900) {
            s += "CM";
            input -= 900;
        }
        while (input >= 500) {
            s += "D";
            input -= 500;
        }
        while (input >= 400) {
            s += "CD";
            input -= 400;
        }
        while (input >= 100) {
            s += "C";
            input -= 100;
        }
        while (input >= 90) {
            s += "XC";
            input -= 90;
        }
        while (input >= 50) {
            s += "L";
            input -= 50;
        }
        while (input >= 40) {
            s += "XL";
            input -= 40;
        }
        while (input >= 10) {
            s += "X";
            input -= 10;
        }
        while (input >= 9) {
            s += "IX";
            input -= 9;
        }
        while (input >= 5) {
            s += "V";
            input -= 5;
        }
        while (input >= 4) {
            s += "IV";
            input -= 4;
        }
        while (input >= 1) {
            s += "I";
            input -= 1;
        }
        return s;
    }

    public static int RomanToNumber(String roman) {

        int number = 0;
        for (int x = 0; x < roman.length(); x++) {

            if (x == 0) {
                if (roman.charAt(x) == 'M') {
                    number = 1000;
                } else if (roman.charAt(x) == 'D') {
                    number = 500;
                } else if (roman.charAt(x) == 'C') {
                    number = 100;
                } else if (roman.charAt(x) == 'L') {
                    number = 50;
                } else if (roman.charAt(x) == 'X') {
                    number = 10;
                } else if (roman.charAt(x) == 'V') {
                    number = 5;
                } else {
                    System.out.print("wrong input\n");
                }

            } else {
                if (roman.charAt(x) == 'M') {
                    if (roman.charAt(x - 1) == 'M') {
                        number += 1000;
                    } else {
                        number = 1000 - number;
                    }
                } else if (roman.charAt(x) == 'D') {
                    if (roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M') {
                        number += 500;
                    } else {
                        if (roman.charAt(x - 1) == 'C') {
                            number = 500 - 100 + number;
                        }
                        if (roman.charAt(x - 1) == 'L') {
                            number = 500 - 50 + number;
                        }
                        if (roman.charAt(x - 1) == 'X') {
                            number = 500 - 10 + number;
                        }
                        if (roman.charAt(x - 1) == 'V') {
                            number = 500 - 5 + number;
                        }
                        if (roman.charAt(x - 1) == 'I') {
                            number = 500 - 1 + number;
                        }
                    }

                } else if (roman.charAt(x) == 'C') {
                    if (roman.charAt(x - 1) == 'C' || roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M') {
                        number += 100;
                    } else {
                        if (roman.charAt(x - 1) == 'L') {
                            number = 100 - 50 + number;
                        }
                        if (roman.charAt(x - 1) == 'X') {
                            number = 100 - 10 + number;
                        }
                        if (roman.charAt(x - 1) == 'V') {
                            number = 100 - 5 + number;
                        }
                        if (roman.charAt(x - 1) == 'I') {
                            number = 100 - 1 + number;
                        }
                    }

                } else if (roman.charAt(x) == 'L') {
                    if (roman.charAt(x - 1) == 'C' || roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M' || roman.charAt(x - 1) == 'L') {
                        number += 50;
                    } else {
                        if (roman.charAt(x - 1) == 'X') {
                            number = 50 - 10 + number;
                        }
                        if (roman.charAt(x - 1) == 'V') {
                            number = 50 - 5 + number;
                        }
                        if (roman.charAt(x - 1) == 'I') {
                            number = 50 - 1 + number;
                        }
                    }
                } else if (roman.charAt(x) == 'X') {
                    if (roman.charAt(x - 1) == 'X' || roman.charAt(x - 1) == 'C' || roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M' || roman.charAt(x - 1) == 'L') {
                        number += 10;
                    } else {
                        if (roman.charAt(x - 1) == 'V') {
                            number = 10 - 5 + number;
                        }
                        if (roman.charAt(x - 1) == 'I') {
                            number = 10 - 1 + number;
                        }
                    }
                } else if (roman.charAt(x) == 'V') {
                    if (roman.charAt(x - 1) == 'V' || roman.charAt(x - 1) == 'X' || roman.charAt(x - 1) == 'C' || roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M' || roman.charAt(x - 1) == 'L') {
                        number += 5;
                    } else {
                        if (roman.charAt(x - 1) == 'I') {
                            number = 5 - 1 + number;
                        }
                    }
                } else {
                    if (roman.charAt(x - 1) == 'I' || roman.charAt(x - 1) == 'V' || roman.charAt(x - 1) == 'X' || roman.charAt(x - 1) == 'C' || roman.charAt(x - 1) == 'D' || roman.charAt(x - 1) == 'M' || roman.charAt(x - 1) == 'L') {
                        number += 1;
                    }
                }

            }
        }

        return number;
    }
}

