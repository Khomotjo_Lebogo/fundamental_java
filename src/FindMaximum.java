/**
 * Created by intern on 9/21/15.
 */
public class FindMaximum {
    public static void main(String[] args) {

        int[] numbers = {88, 6, 34, 500, 33, 55, 23, 64, 123};

        int maximum = Integer.MIN_VALUE;


        for (int i : numbers) {

            if (i > maximum) {
                maximum = i;
            }
        }


        System.out.println("maximum number in array is : " + maximum);
    }
}