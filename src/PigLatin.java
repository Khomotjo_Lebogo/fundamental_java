/**
 * Created by intern on 10/7/15.
 */

import javax.swing.*;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class PigLatin {
    public static final String vowels = "-way";
    public static final String contsance = "ay";
    public static char firrstchar[] = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
    public static Scanner scan = new Scanner(System.in);
    public static StringBuilder buffer = new StringBuilder();
    public static StringBuilder builder2 = new StringBuilder();
    public static StringBuilder answer = new StringBuilder();
    public static String ans = "";

    public static void main(String args[]) {

        System.out.print("Would you like to convert to pig style from english?\nplease press(1) if convert from English to Piglatin or (2) if converting from PigLatin to English?");
        String input = scan.next();

        if (input.equals("1")) {
            System.out.print("Enter word you want to convert?:");
            String word = scan.next();

            if (word.charAt(0) == 'a' || word.charAt(0) == 'e' || word.charAt(0) == 'i' || word.charAt(0) == 'o' || word.charAt(0) == 'u') {
                ans = vowelsFunctions(word);
                System.out.print(ans);
            } else {
                answer = Conse(word);
                System.out.print(answer);
            }
        } else if (input.equals("2")) {
            System.out.print("Enter word you want to convert?:");
            String word = scan.next();
            StringBuilder builder = convertToNormal(word);
            System.out.println((builder));
        } else {
            System.out.print("Enter (1) or (2)......Now you Exiting the system!");
        }
    }

    public static String vowelsFunctions(String input) {
        input = input + vowels;
        return input;
    }

    public static StringBuilder Conse(String input) {
        String Temp = "";
        MainLoop:
        for (int charactersInput = 0; charactersInput < input.length(); charactersInput++) {
            InnerLoop:
            for (int x = 0; x < firrstchar.length; x++) {
                if (input.charAt(charactersInput) == firrstchar[x]) break MainLoop;
                {
                    Temp += input.charAt(charactersInput);
                }
            }
        }
        Set<Character> set = new LinkedHashSet<>();
        for (int i = 0; i < Temp.length(); i++) {
            set.add(Temp.charAt(i));
            buffer = new StringBuilder(set.size());
            for (Character character : set) {
                buffer.append(character);
            }
            int bufferLength = buffer.length();
            buffer.deleteCharAt(bufferLength - 1);
            builder2 = new StringBuilder(input);
            builder2.delete(0, bufferLength - 1);
            builder2.append("-" + buffer + contsance);
        }
        return builder2;
    }

    public static StringBuilder convertToNormal(String input) {
        StringBuilder builder = new StringBuilder(input);
        int lengthOfinput = builder.length();
        int indexOfDash = builder.indexOf("-");

        if (indexOfDash < 0) {
            System.out.print("not pig style....");
            System.exit(0);
        } else if (builder.charAt(indexOfDash + 1) == 'w') {
            builder.delete(indexOfDash, lengthOfinput);
        } else if (builder.charAt(indexOfDash + 1) != ' ' && builder.charAt(indexOfDash + 1) != 'w') {
            String deletedPart = builder.substring(indexOfDash + 1, lengthOfinput - 2);
            builder = builder.delete(indexOfDash, lengthOfinput);
            builder.insert(0, deletedPart);

        }

        return builder;
    }
}