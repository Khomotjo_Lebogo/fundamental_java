/**
 * Created by intern on 10/7/15.
 */
import java.util.Scanner;
public class GoldBachConjecture {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter number : ");
        String number=scanner.next();
        int N = Integer.parseInt(number);

        boolean[] isprime = new boolean[N];

        for (int i = 2; i < N; i++)
          // if (isprime % 2 == 0) {
                isprime[i] = true;



        for (int i = 2; i * i < N; i++) {
            if (isprime[i]) {
                for (int j = i; i * j < N; j++)
                    isprime[i*j] = false;
            }
        }

        int primes = 0;
        for (int i = 2; i < N; i++)
            if (isprime[i]) primes++;

        System.out.println("primes of entered number : " );

        int[] list = new int[primes];
        int n = 0;
        for (int i = 0; i < N; i++)
            if (isprime[i]) list[n++] = i;

        int left = 0, right = primes-1;
        while (left <= right) {
            if      (list[left] + list[right] == N) break;
            else if (list[left] + list[right]  < N) left++;
            else right--;
        }
        if (list[left] + list[right] == N)
            System.out.println(N + " = " + list[left] + " + " + list[right]);
        else
            System.out.println(N + " not expressible as sum of two primes");
    }

}
