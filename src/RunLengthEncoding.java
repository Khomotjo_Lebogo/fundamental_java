/**
 * Created by intern on 10/7/15.
 */
/*import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class RunLengthEncoding {

    public static String encode(String source) {
        AtomicReference<StringBuilder> dest;
        dest = new AtomicReference<>(new StringBuilder());
        for (int i = 0; i < source.length(); i++) {
            int runLength = 1;
            while (i+1 < source.length() && source.charAt(i) == source.charAt(i+1)) {
                runLength++;
                i++;
            }
            dest.get().append(runLength);
            dest.get().append(source.charAt(i));
        }
        return dest.get().toString();
    }

    /*public static String decode(String source) {
        StringBuffer dest = new StringBuffer();
        Pattern pattern = Pattern.compile("[0-9]+|[a-z,A-Z]");
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            int number = Integer.parseInt(matcher.group());
            matcher.find();
            while (number-- != 0) {
                dest.append(matcher.group());
            }
        }
        return dest.toString();
    }
*/
   /* public static void main(String[] args) {
        String example = "ABBB~CDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE";
        System.out.println(encode(example));
        //System.out.println(decode(""));
    }
}*/
import java.util.Scanner;

/*public class RunLengthEncoding {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter string: ");
        String str = sc.nextLine();
        System.out.println("Input: " + str);

        String compressed = "";

        char ch=0;
        int count=1;
        for (int x = 0; x < str.length(); x++) {
            if (ch == str.charAt(x)){
                count = count + 1;
            } else {
                compressed = compressed + ch;
                if(count != 1){
                    compressed = compressed + count;
                }
                ch = str.charAt(x);
                count = 1;
            }
        }
        compressed = compressed + ch;
        if(count != 0){
            compressed = compressed + count;
        }
        System.out.println("Compressed: " + compressed);
    }
}*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class RunLengthEncoding {

    public static String encode(String source) {
        StringBuffer dest = new StringBuffer();
        for (int i = 0; i < source.length(); i++) {
            int runLength = 1;
            while (i+1 < source.length() && source.charAt(i) == source.charAt(i+1)) {
                runLength++;
                i++;
            }
            dest.append(runLength);
            dest.append(source.charAt(i));
        }
        return dest.toString();
    }

    public static String decode(String source) {
        StringBuffer dest = new StringBuffer();
        Pattern pattern = Pattern.compile("[0-9]+|[a-zA-Z]");
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            int number = Integer.parseInt(matcher.group());
            matcher.find();
            while (number-- != 0) {
                dest.append(matcher.group());
            }
        }
        return dest.toString();
    }

    public static void main(String[] args) {
        String example = " ABBB~CDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE";
        System.out.println(encode(example));
        System.out.println(decode(" "));
    }
}