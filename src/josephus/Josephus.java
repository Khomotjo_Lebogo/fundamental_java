package josephus;

/**
 * Created by intern on 10/7/15.
 */import josephus.CircularLinkedList;
import josephus.CircularLinkedListCircularIterator;

import java.util.ArrayList;
import java.util.Scanner;


public class Josephus {


    public static void main(String[] args) {
        Scanner consoleScanner = new Scanner(System.in);
        System.out.println("How many people you wanna kill?");
        int n = consoleScanner.nextInt();
        System.out.println("Each time you kill someone, it'll be the nth person in line. What is n?");
        int m = consoleScanner.nextInt();

        for (int next : josephus(n, m)) {
            System.out.println("Kill person #" + next);
        }
    }


    public static ArrayList<Integer> josephus(int n, int m) {
        CircularLinkedList<Integer> josephusList = new CircularLinkedList<Integer>();

        int i;
        for (i = 0; i < n; ++i) {
            josephusList.addNode(i);
        }

        ArrayList<Integer> result = new ArrayList<Integer>(n);
        CircularLinkedListCircularIterator<Integer> iterator = josephusList.iterator();
        int j, person = 0;


        for (i = 0; i < n; ++i) {
            j = 1;
            while (j < m) {
                person = iterator.next();
                ++j;
            }
            iterator.remove();
            result.add(person);
        }

        return result;
    }
}