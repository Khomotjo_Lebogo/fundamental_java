/**
 * Created by intern on 10/06/15.
 */
public class CallingMethod {
    public static void main(String[] args) {
        String name = "", surname = "";
        CallingMethod objCallingMethod = new CallingMethod();

        noArguments();
        fixedArguments(name, surname);
        System.out.println("I am an intern at: " + returnValue());
        displayPrivate();

        objCallingMethod.optionalArguments("Digital", "Geekaship");
        objCallingMethod.optionalArguments("programming");

        System.out.println();
        EmailAddress objEmail = new EmailAddress();
        System.out.println(objEmail.email); //Pass by value

        otherEmail(objEmail);
        System.out.println(objEmail.email); //Pass by reference
    }

    public static void noArguments() {
        System.out.println("This is a method with no arguments");
    }

    public static void fixedArguments(String name, String surname) {
        name = "Khomotjo";
        surname = "Lebogo";
        System.out.println("My name: " + name);
        System.out.println("My surname: " + surname);
    }

    public static void optionalArguments(String lineOne, String lineTwo) {
        System.out.println(lineOne + " " + lineTwo);
    }

    public static void optionalArguments(String lineThree) {
        System.out.println(lineThree);
    }

    public static String returnValue() {
        String company = "Infoware Studios";
        return company;
    }

    public static void otherEmail(EmailAddress mail) {
        mail.email = "lebogoke@gmail.com";
    }

    private static void displayPrivate() {
        System.out.println("This is a private method");
        displayProtected();
    }

    protected static void displayProtected() {
        System.out.println("This is a protected method\n");
    }
}

class EmailAddress {
    public String email = "eagnolia@ymail.com";
}