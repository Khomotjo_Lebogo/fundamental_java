import java.util.List;

/**
 * Created by intern on 9/17/15.
 */
public class AddingArrayNumbers {

    public static void main(String args[]) {


        int numbers[] = {1, 0, 13, 43, 5, 36, 7, 6, 88, 10};
        int sum = 0;

        for (int i : numbers)
            sum += i;

        System.out.println(sum);
    }
}