/**
 * Created by intern on 9/23/15.
 */

import java.text.MessageFormat;

public class Bottle99 {

    static String bottles(final int n) {
        return MessageFormat.format("{0,choice,0#No more bottles|1#One bottle|2#{0} bottles} of beer", n);
    }

    public static void main(final String[] args) {
        String bottle = bottles(99);

        for (int x = 99; x > 0; ) {
            System.out.println(bottle + " on the wall");
            System.out.println(bottle);
            System.out.println("Take one down, pass it around");
            bottle = bottles(--x);
            System.out.println(bottle + " on the wall\n");
        }
    }
}
