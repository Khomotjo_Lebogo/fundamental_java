/**
 * Created by intern on 9/28/15.
 */
import java.io.File;
public class deleteFile {

   public static final String FILE_PATH = "/Users/intern/Desktop/fundamental/fundamental_java/src/Deleting";

    public static void main(String[] args) {

        if (deleteFile.deleteFile(FILE_PATH)) {
            System.out.println("File is deleted!");
        } else {
            System.err.println("Failed to delete file.");
        }
    }

    public static boolean deleteFile(String filePath) {

        File file = new File(FILE_PATH);
        return file.delete();
    }
}